'use strict'

const R = require('ramda');
const { MongoClient } = require('mongodb');
const uuid = require('uuid');
const { makeExecutableSchema } = require('graphql-tools');

const typeDefs = `
  input UserInput {
      name: String
      email: String!
      username: String!
      accounts: [AccountInput]
  }

  input LocationInput {
      geo: [Float]!
  }

  input UserUpdateInput {
      name: String
      email: String
      userName: String
  }

  input AccountInput {
      sourceSystem: String!
      accountName: String!
  }

  type PoliticalLocation {
      City: String!
      State: String
      Country: String!
  }

  type Location {
      geo: [Float]!
      political: PoliticalLocation
  }

  type User {
      id: ID!
      name: String
      email: String!
      username: String!
      accounts: [Account]!
      location: Location
      stats: [Stat]
  }

  type Account {
      sourceSystem: String!
      accountName: String!
  }

  type Stat {
      type: String
      name: String!
      total: Float!
  }

  type Sources {
      name: String!
      url: String
  }

  type Query {
    users: [User]
    user(username: String!): User
  }

  type Mutation {
    addUser(user: UserInput): User
    removeUser(userId: ID!): Boolean
    addAccount(userId: ID!, account: AccountInput!): Boolean
    removeAccount(userId: ID!, sourceSystem: String!, accountName: String): Boolean
  }
`;

function mapSchema(entry) {
    return entry? R.assoc('id', entry._id, R.omit('_id', entry)) : null;
}


function mapInput(entry) {
    return entry? R.assoc('_id', entry.id, R.omit('id', entry)) : null;
}

function findAndMap(col, criteria) {
    return col.find(criteria).toArray().then(arr => R.map(mapSchema, arr));
}

function findOne(col, criteria) {
    return col.findOne(criteria).then(mapSchema);
}

function getAll(col) {
    return findAndMap(col, {})
}

function getById(col, id) {
    return col.findOne({ _id: id }).then(mapSchema);
}

function add(col, entry) {
    const fullEntry = R.assoc('id', uuid.v4(), entry);
    return col.insertOne(mapInput(fullEntry))
    .then(result => {
        return fullEntry;
    })
}

function mapUserInput(input) {
    return R.merge({ accounts: [], stats: [] }, input);
}

function addToInnerCollection(col, id, item, path) {
    return col.updateOne(
        { _id : id},
        {
            $push: R.assoc(path, item, {})
        });
}

function removeFromInnerCollection(col, id, itemMatcher, path) {
    return col.updateOne(
        { _id : id},
        {
            $pull: R.assoc(path, itemMatcher, {})
        });
}


function remove(col, id) {
    return col.deleteOne({_id : id});
}

const mapStats = R.pipe(
    // first, merge stats from all sources
    // into a single object with all totals
    R.reduce(R.mergeWith(R.add), {}), 
    // then, break resulting object into pairs,
    // to follow the 'Stats' contract
    R.toPairs, 
    // then turn each pair a 'Stats' entry
    R.map(
        R.pipe(
        // first we apply a different function for each element in the array
        R.zipWith(R.call, [
            R.pipe(
            // splitting the key name (eg: repo.count)
            R.split('.'), 
            // first argument is the type, second is the name
            R.zipObj(['type', 'name'])
            ),
            // turning the value (eg: repo.count: _7_) into a property
            R.objOf('total')
        ]),
        // then we merge the resulting objects
        R.mergeAll
        )
    )
)

module.exports.createSchema = (config, adapters) => {
    return MongoClient.connect(config.mongodbUrl)
    .then(db => {
        const Users = db.collection('users');
        const resolvers = {
            Query: {
                users: () => getAll(Users),
                user:  (_, { username }) => findOne(Users, { username })
            },
            User: {
                stats(user) {
                    return new Promise((resolve, reject) => {
                        Promise.all(
                            R.map(acct => adapters[acct.sourceSystem].getUserStats(acct.accountName), user.accounts || []))
                        .then((response) => {
                            resolve(mapStats(response));
                        })
                        .catch(reject);
                    });
                }
            },
            Mutation: {
                addUser: (_, { user }) => add(Users, mapUserInput(user)),
                removeUser: (_, { userId }) => remove(Users, userId),
                addAccount: (_, { userId, account }) => addToInnerCollection(Users, userId, account, 'accounts'),
                removeAccount: (_, { userId, sourceSystem, accountName }) => removeFromInnerCollection(Users, userId, { sourceSystem, accountName }, 'accounts'),
            }
        };

        const schema = makeExecutableSchema({
            typeDefs,
            resolvers,
        });

        return schema;
    })
}
