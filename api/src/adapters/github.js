'use strict'

const { GraphQLClient } = require('graphql-request');

module.exports.createAdapter = (config) => {
    this.client = new GraphQLClient(config.url, {headers: {Authorization: `Bearer ${config.token}`}});
    this.result = {};
    this.getUserStats = (userName) => {       
        const query = `{
            user(login:"${userName}"){
                forks: repositories(isFork:true) {
                    totalCount
                },
                repositories (isFork: false) {
                    totalCount
                },
                starredRepositories {
                    totalCount
                },
                watching {
                    totalCount
                }
            }
        }`;

        return this.client.request(query).then(response => {
                return {
                    'repo.count': response.user.repositories.totalCount,
                    'repo.stars': response.user.starredRepositories.totalCount,
                    'repo.forks': response.user.forks.totalCount,
                    'repo.watching': response.user.watching.totalCount
                };
            }).catch(console.error);
    }

    this.getUserStats = this.getUserStats.bind(this);

    return this;
}
