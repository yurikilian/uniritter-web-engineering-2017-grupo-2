'use strict'

const got = require('got');

const printe = function (result, inner) {
    console.log("Result ->", result.body);
    console.log("Inner ->", inner.body);
}

module.exports.createAdapter = (config) => {

    this.fetchData = function() {
        let goFetch = function(repositories, url) {
            return got(url || (config.url + `/repositories/kylesmiff`)).then(function(data) {
                let body = JSON.parse(data.body);
                repositories.push(body);
            
                if(body.next) {
                    return goFetch(repositories, body.next);
                } else {
                    return repositories;
                }
            });
        }

        return goFetch([], null);
    };

    
    this.getUserStats = (userName) => {           
        return this.fetchData().then(result => {
            return {
                'repo.count': result.map(page => page.values.length).reduce((acum, len) => acum +len),
                'repo.stars': 0,
                'repo.forks': 0,
                'repo.watching': 0
            };

        }).catch(console.error);
        


    }

    this.getUserStats = this.getUserStats.bind(this);
    this.fetchData = this.fetchData.bind(this);
    return this;
}
