'use strict'

var { defineSupportCode } = require('cucumber');
const expect = require('chai').expect;

defineSupportCode(function ({ Given, Then, When }) {
    Given('a running, healthy API', function () {
        // this step has no action -- the API is already running
    });

    Then('a green status message', function () {
        expect(this.response.body.status).to.equal('green');
    });    
});
